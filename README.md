# go-book

Created with Node,Express and Pug as part of [The Odin Project Curriculum](https://www.theodinproject.com/courses/nodejs/lessons/https://www.theodinproject.com/courses/nodejs/lessons/inventory-application).

I deviated from Odin project by creating a compendium for [Go](<https://en.wikipedia.org/wiki/Go_(game)>) instead of an item inventory.

# Completed Extra Credit

Allow image upload
Save images in MongoDB
Require User Account to modify info

# Final Thoughts

Completing this project was a tool for me to practice with Node concepts and the Express framework.
