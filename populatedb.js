#! /usr/bin/env node

console.log(
  'This script populates some test players and matches to your database. Specified database as argument - e.g.: populatedb mongodb+srv://cooluser:coolpassword@cluster0-mbdj7.mongodb.net/go-book?retryWrites=true'
);

// Get arguments passed on command line
const userArgs = process.argv.slice(2);
/*
  if (!userArgs[0].startsWith('mongodb')) {
      console.log('ERROR: You need to specify a valid mongodb URL as the first argument');
      return
  }
  */
const async = require('async');
const mongoose = require('mongoose');
const Player = require('./models/player');
const Match = require('./models/match');

const mongoDB = userArgs[0];
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const players = [];
const matches = [];

function playerCreate(
  firstName,
  familyName,
  dBirth,
  dDeath,
  country,
  rank,
  matchesPlayed,
  cb
) {
  const playerDetail = {
    first_name: firstName,
    family_name: familyName,
    country,
    rank,
  };
  if (dBirth !== false) playerDetail.date_of_birth = dBirth;
  if (dDeath !== false) playerDetail.date_of_death = dDeath;
  if (matchesPlayed !== false) playerDetail.matchesPlayed = matchesPlayed;

  const player = new Player(playerDetail);

  player.save(function(err) {
    if (err) {
      cb(err, null);
      return;
    }
    console.log(`New Player: ${player}`);
    players.push(player);
    cb(null, player);
  });
}

function matchCreate(
  dMatch,
  winner,
  scoreDifference,
  whitePlayer,
  blackPlayer,
  description,
  matchEvent,
  cb
) {
  const matchDetail = {};
  if (dMatch !== false) matchDetail.date_of_match = dMatch;
  if (winner !== 'Draw') matchDetail.winner = winner;
  if (whitePlayer !== false) matchDetail.white_player = whitePlayer;
  if (blackPlayer !== false) matchDetail.black_player = blackPlayer;
  if (description !== false) matchDetail.description = description;
  if (scoreDifference !== false)
    matchDetail.winner_score_difference = scoreDifference;
  if (matchEvent !== false) matchDetail.match_event = matchEvent;

  const match = new Match(matchDetail);
  match.save(function(err) {
    if (err) {
      cb(err, null);
      return;
    }
    console.log(`New Match: ${match}`);
    matches.push(match);
    cb(null, match);
  });
}

function createPlayers(cb) {
  async.series(
    [
      function(callback) {
        playerCreate(
          'Go',
          'Seigan',
          '1914-06-12',
          '2014-11-30',
          'China',
          '9 dan',
          false,
          callback
        );
      },
      function(callback) {
        playerCreate(
          'Honinbo',
          'Shusai',
          '1874-06-24',
          '1940-01-18',
          'Japan',
          '9 dan',
          false,
          callback
        );
      },
      function(callback) {
        playerCreate(
          'Kaku',
          'Takagawa',
          '1915-09-21',
          '1986-11-26',
          'Japan',
          '9 dan',
          false,
          callback
        );
      },
    ],
    // optional callback
    cb
  );
}

function createMatches(cb) {
  async.parallel(
    [
      function(callback) {
        matchCreate(
          '1933-10-16',
          players[1],
          '2',
          players[1],
          players[0],
          'The Game of the Century',
          '',
          callback
        );
      },
      function(callback) {
        matchCreate(
          '1972-11-06',
          players[0],
          '6.5',
          players[2],
          players[0],
          '',
          'Oza, 20th',
          callback
        );
      },
    ],
    // optional callback
    cb
  );
}

async.series(
  [createPlayers, createMatches],
  // Optional callback
  function(err, results) {
    if (err) {
      console.log(`FINAL ERR: ${err}`);
    }
    // All done, disconnect from database
    mongoose.connection.close();
  }
);
