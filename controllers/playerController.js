const { body, validationResult, sanitizeBody } = require('express-validator');

const async = require('async');

const Player = require('../models/player');
const Match = require('../models/match');

exports.index = function(req, res) {
  async.parallel(
    {
      player_count(callback) {
        Player.countDocuments({}, callback); // empty object to find all
      },
      match_count(callback) {
        Match.countDocuments({}, callback);
      },
    },
    function(err, results) {
      res.render('index', { title: 'Go Book Home', error: err, data: results });
    }
  );
};

// display list of all Players
exports.player_list = function(req, res, next) {
  Player.find({})
    .populate('match')
    .exec(function(err, listPlayers) {
      if (err) {
        return next(err);
      }
      res.render('player_list', {
        title: 'Player List',
        player_list: listPlayers,
      });
    });
};

// display detail page for a specific player
exports.player_detail = function(req, res, next) {
  async.parallel(
    {
      player(callback) {
        Player.findById(req.params.id).exec(callback);
      },
      matches(callback) {
        Match.find({
          $or: [
            { white_player: req.params.id },
            { black_player: req.params.id },
          ],
        })
          .populate('white_player')
          .populate('black_player')
          .populate('winner')
          .exec(callback);
      },
    },
    function(err, results) {
      if (err) {
        return next(err);
      }
      if (results.player == null) {
        // no results
        const err = new Error('Player not found');
        err.status = 404;
        return next(err);
      }
      // success
      res.render('player_detail', {
        title: 'Player Info',
        player: results.player,
        matches: results.matches,
      });
    }
  );
};

// display Player create form on GET
exports.player_create_get = function(req, res, next) {
  res.render('player_form', { title: 'Create Player' });
};

// handle Player create on POST
exports.player_create_post = [
  // validate fields
  body('first_name')
    .isLength({ min: 1 })
    .trim()
    .withMessage('First name must be specified')
    .isAlphanumeric()
    .withMessage('First name has non-alphanumeric characters.'),
  body('family_name')
    .isLength({ min: 1 })
    .trim()
    .withMessage('Last name must be specified')
    .isAlphanumeric()
    .withMessage('Last name has non-alphanumeric characters.'),
  body('date_of_birth', 'Invalid date of birth')
    .optional({ checkFalsy: true })
    .isISO8601(),
  body('date_of_death', 'Invalid date of death')
    .optional({ checkFalsy: true })
    .isISO8601(),
  body('country')
    .isLength({ min: 1 })
    .trim()
    .withMessage('Country must be specified')
    .isAlphanumeric()
    .withMessage('Country has non-alphanumeric characters.'),
  body('rank')
    .isLength({ min: 1 })
    .trim()
    .withMessage('Rank must be specified'),

  // Sanitize fields (using wildcard)
  sanitizeBody('*').escape(),

  // process request after validation and sanitization
  (req, res, next) => {
    // extract the validation errors from a request
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.render('player_form', {
        title: 'Create Player',
        player: req.body,
        errors: errors.array(),
      });
    } else {
      // data is valid
      // create a Player object with escaped and trimmed data
      const player = new Player({
        first_name: req.body.first_name,
        family_name: req.body.family_name,
        date_of_birth: req.body.date_of_birth,
        date_of_death: req.body.date_of_death,
        country: req.body.country,
        rank: req.body.rank,
      });
      player.save(function(err) {
        if (err) {
          return next(err);
        }
        // success
        res.redirect(player.url);
      });
    }
  },
];

// display Player delete form on GET
exports.player_delete_get = function(req, res, next) {
  async.parallel(
    {
      player(callback) {
        Player.findById(req.params.id).exec(callback);
      },
      players_matches(callback) {
        Match.find({
          $or: [
            { white_player: req.params.id },
            { black_player: req.params.id },
          ],
        })
          .populate('white_player')
          .populate('black_player')
          .populate('winner')
          .exec(callback);
      },
    },
    function(err, results) {
      if (err) {
        return next(err);
      }
      if (results.player === null) {
        res.redirect('/compendium/players');
      }
      // success
      res.render('player_delete', {
        title: 'Delete Player',
        player: results.player,
        player_matches: results.players_matches,
      });
    }
  );
};

// handle Player delete on POST
exports.player_delete_post = function(req, res, next) {
  async.parallel(
    {
      player(callback) {
        Player.findById(req.body.playerid).exec(callback);
      },
      players_matches(callback) {
        Match.find({
          $or: [
            { white_player: req.body.playerid },
            { black_player: req.body.playerid },
          ],
        }).exec(callback);
      },
    },
    function(err, results) {
      if (err) {
        return next(err);
      }
      // success
      if (results.players_matches.length > 0) {
        // Player has matches
        res.render('player_delete', {
          title: 'Delete Player',
          player: results.player,
          player_matches: results.players_matches,
        });
      } else {
        // Player has no matches, delete
        Player.findByIdAndRemove(req.body.playerid, function deletePlayer(err) {
          if (err) {
            return next(err);
          }
          // success
          res.redirect('/compendium/players');
        });
      }
    }
  );
};

// display Player update form on GET
exports.player_update_get = function(req, res, next) {
  async.parallel(
    {
      player(callback) {
        Player.findById(req.params.id).exec(callback);
      },
    },
    function(err, results) {
      if (err) {
        return next(err);
      }
      if (results.player == null) {
        const err = new Error('Player not found');
        err.status = 404;
        return next(err);
      }
      // success
      res.render('player_form', {
        title: 'Update Player',
        player: results.player,
      });
    }
  );
};

// handle Player update on POST
exports.player_update_post = [
  // validate fields
  body('first_name')
    .isLength({ min: 1 })
    .trim()
    .withMessage('First name must be specified')
    .isAlphanumeric()
    .withMessage('First name has non-alphanumeric characters.'),
  body('family_name')
    .isLength({ min: 1 })
    .trim()
    .withMessage('Last name must be specified')
    .isAlphanumeric()
    .withMessage('Last name has non-alphanumeric characters.'),
  body('date_of_birth', 'Invalid date of birth')
    .optional({ checkFalsy: true })
    .isISO8601(),
  body('date_of_death', 'Invalid date of death')
    .optional({ checkFalsy: true })
    .isISO8601(),
  body('country')
    .isLength({ min: 1 })
    .trim()
    .withMessage('Country must be specified')
    .isAlphanumeric()
    .withMessage('Country has non-alphanumeric characters.'),
  body('rank')
    .isLength({ min: 1 })
    .trim()
    .withMessage('Rank must be specified'),

  // Sanitize fields (using wildcard)
  sanitizeBody('*').escape(),

  // process request after validation and sanitization
  (req, res, next) => {
    // extract the validation errors from a request
    const errors = validationResult(req);
    // data is valid
    // create a Player object with escaped and trimmed data
    const player = new Player({
      first_name: req.body.first_name,
      family_name: req.body.family_name,
      date_of_birth: req.body.date_of_birth,
      date_of_death: req.body.date_of_death,
      country: req.body.country,
      rank: req.body.rank,
      _id: req.params.id,
    });

    if (!errors.isEmpty()) {
      // errors, so render form again
      res.render('player_form', {
        title: 'Update Player',
        player,
        errors: errors.array(),
      });
    } else {
      // form data is valid
      Player.findByIdAndUpdate(req.params.id, player, {}, function(
        err,
        theplayer
      ) {
        if (err) {
          return next(err);
        }
        // success
        res.redirect(theplayer.url);
      });
    }
  },
];
