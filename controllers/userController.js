const bcrypt = require('bcryptjs');
const User = require('../models/user');

exports.user_create_get = (req, res, next) => {
  res.render('sign-up-form', { title: 'Sign Up' });
};

exports.user_create_post = (req, res, next) => {
  bcrypt.hash(req.body.password, 12, (err, hashedPassword) => {
    if (err) {
      return next(err);
    }
    // otherwise, store hashedPassword in DB
    new User({
      username: req.body.username,
      password: hashedPassword,
    }).save(err => {
      if (err) {
        return next(err);
      }
      res.redirect('/');
    });
  });
};

exports.user_login_get = (req, res, next) => {
  res.render('login-form', { title: 'Log In' });
};

exports.user_login_post = (res, next) => {
  // res.send(req.body);
};

exports.logout_get = (req, res, next) => {
  req.logout();
  res.redirect('/');
};
