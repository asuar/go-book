const async = require('async');
const { body, validationResult, sanitizeBody } = require('express-validator');

const GridFsStorage = require('multer-gridfs-storage');
const multer = require('multer');
const crypto = require('crypto');
const path = require('path');
const mongoDB = require('../config/mongoDB');

const Player = require('../models/player');
const Match = require('../models/match');

// Create storage engine
const storage = new GridFsStorage({
  url: process.env.MONGODB_URI_DEV,
  file: (req, file) =>
    new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        const filename = buf.toString('hex') + path.extname(file.originalname);
        const fileInfo = {
          filename,
          bucketName: 'uploads',
        };
        resolve(fileInfo);
      });
    }),
});

const upload = multer({ storage });

// display list of all Matches
exports.match_list = function(req, res, next) {
  Match.find()
    .populate('winner')
    .populate('white_player')
    .populate('black_player')
    .exec(function(err, listMatches) {
      if (err) {
        return next(err);
      }
      res.render('match_list', {
        title: 'Match List',
        match_list: listMatches,
      });
    });
};

// display detail page for a specific match
exports.match_detail = function(req, res, next) {
  async.parallel(
    {
      match(callback) {
        Match.findById(req.params.id)
          .populate('winner')
          .populate('white_player')
          .populate('black_player')
          .exec(callback);
      },
    },
    function(err, results) {
      if (err) {
        return next(err);
      }
      if (results.match === null) {
        // no results
        const err = new Error('Match not found');
        err.status = 404;
        return next(err);
      }
      // successful so render
      res.render('match_detail', {
        title: 'Match Information',
        match: results.match,
      });
    }
  );
};

// display Match create form on GET
exports.match_create_get = function(req, res, next) {
  Player.find({}).exec(function(err, players) {
    if (err) {
      return next(err);
    }
    // success
    res.render('match_form', { title: 'Create Match', player_list: players });
  });
};

// handle Match create on POST
exports.match_create_post = [
  upload.single('game_image'),
  //  validate fields
  body('date_of_match', 'Invalid date')
    .optional({ checkFalsy: true })
    .isISO8601(),
  body('winner', 'Winner must not be empty.')
    .isLength({ min: 1 })
    .trim(),
  body('score_difference', 'Score difference must not be empty.')
    .isLength({ min: 1 })
    .trim(),
  body('white_player', 'White must not be empty.')
    .isLength({ min: 1 })
    .trim(),
  body('black_player', 'Black must not be empty.')
    .isLength({ min: 1 })
    .trim(),
  body('description')
    .isLength({ min: 1 })
    .trim(),
  body('match_event')
    .isLength({ min: 1 })
    .trim(),

  // Sanitize fields (using wildcard)
  sanitizeBody('*').escape(),

  // process request after validation and sanitization
  (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.render('match_form', {
        title: 'Create Match',
        match: req.body,
        errors: errors.array(),
      });
    } else {
      // data is valid
      let winner;
      switch (req.body.winner) {
        case 'W':
          winner = req.body.white_player;
          break;
        case 'B':
          winner = req.body.black_player;
          break;
        case 'D':
          winner = null;
          break;
        default:
          winner = null;
          break;
      }
      // create a Match object with escaped and trimmed data
      const match = new Match({
        date_of_match: req.body.date_of_match,
        winner,
        winner_score_difference: req.body.score_difference,
        white_player: req.body.white_player,
        black_player: req.body.black_player,
        description: req.body.description,
        match_event: req.body.match_event,
        image_file: req.file.filename,
      });
      match.save(function(err) {
        if (err) {
          return next(err);
        }
        // success
        res.redirect(match.url);
      });
    }
  },
];

// display Match delete form on GET
exports.match_delete_get = function(req, res, next) {
  Match.findById(req.params.id).exec(function(err, match) {
    if (err) {
      return next(err);
    }
    // success
    res.render('match_delete', { title: 'Delete Match', match });
  });
};

// handle Match delete on POST
exports.match_delete_post = function(req, res, next) {
  Match.findById(req.params.id).exec(function(err, match) {
    if (err) {
      return next(err);
    }
    // success
    mongoDB.gridFSBucket
      .find({
        filename: match.image_file,
      })
      .toArray(function(err, files) {
        if (files.length > 0) mongoDB.gridFSBucket.delete(files[0]._id);
      });

    Match.findByIdAndRemove(req.body.matchid, function deleteMatch(err) {
      if (err) {
        return next(err);
      }

      // success - return to match list
      res.redirect('/compendium/matches');
    });
  });
};

// display Match update form on GET
exports.match_update_get = function(req, res, next) {
  async.parallel(
    {
      match(callback) {
        Match.findById(req.params.id).exec(callback);
      },
      players(callback) {
        Player.find({}).exec(callback);
      },
    },
    function(err, results) {
      if (err) {
        return next(err);
      }
      if (results.match === null) {
        const err = new Error('Match not found');
        err.status = 404;
        return next(err);
      }
      // success
      res.render('match_form', {
        title: 'Update Match',
        match: results.match,
        player_list: results.players,
      });
    }
  );
};

// handle Match update on POST
exports.match_update_post = [
  upload.single('game_image'),
  //  validate fields
  body('date_of_match', 'Invalid date')
    .optional({ checkFalsy: true })
    .isISO8601(),
  body('winner', 'Winner must not be empty.')
    .isLength({ min: 1 })
    .trim(),
  body('score_difference', 'Score difference must not be empty.')
    .isLength({ min: 1 })
    .trim(),
  body('white_player', 'White must not be empty.')
    .isLength({ min: 1 })
    .trim(),
  body('black_player', 'Black must not be empty.')
    .isLength({ min: 1 })
    .trim(),
  body('description')
    .isLength({ min: 1 })
    .trim(),
  body('match_event')
    .isLength({ min: 1 })
    .trim(),

  // Sanitize fields
  sanitizeBody('date_of_match').toDate(),
  sanitizeBody('winner').escape(),
  sanitizeBody('score_difference').escape(),
  sanitizeBody('white_player').escape(),
  sanitizeBody('black_player').escape(),
  sanitizeBody('description').escape(),
  sanitizeBody('match_event').escape(),

  // process request after validation and sanitization
  (req, res, next) => {
    const errors = validationResult(req);

    if (req.file && req.body.currentImage !== req.file.filename) {
      console.log(mongoDB);
      console.log(mongoDB.gridFSBucket);
      mongoDB.gridFSBucket
        .find({
          filename: path.basename(req.body.currentImage),
        })
        .toArray(function(err, files) {
          if (files.length > 0) mongoDB.gridFSBucket.delete(files[0]._id);
        });
    }

    let winner;
    switch (req.body.winner) {
      case 'W':
        winner = req.body.white_player;
        break;
      case 'B':
        winner = req.body.black_player;
        break;
      case 'D':
        winner = null;
        break;
      default:
        winner = null;
        break;
    }
    // create a Match object with escaped and trimmed data
    const match = new Match({
      date_of_match: req.body.date_of_match,
      winner,
      winner_score_difference: req.body.score_difference,
      white_player: req.body.white_player,
      black_player: req.body.black_player,
      description: req.body.description,
      match_event: req.body.match_event,
      image_file: req.file ? req.file.filename : req.body.currentImage,
      _id: req.params.id,
    });

    if (!errors.isEmpty()) {
      res.render('match_form', {
        title: 'Update Match',
        match,
        errors: errors.array(),
      });
    } else {
      // form data is valid
      Match.findByIdAndUpdate(req.params.id, match, {}, function(
        err,
        thematch
      ) {
        if (err) {
          return next(err);
        }
        // success
        res.redirect(thematch.url);
      });
    }
  },
];
