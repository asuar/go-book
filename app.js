const createError = require('http-errors');
const express = require('express');
const path = require('path');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const compression = require('compression');
const helmet = require('helmet');
const dotenv = require('dotenv');

dotenv.config();

// import the mongoose module
const mongoose = require('mongoose');

const passport = require('passport'); // after all models

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/user');
const compendiumRouter = require('./routes/compendium'); // import routes for compendium area of site

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

const mongoDB = require('./config/mongoDB');

mongoDB.init(mongoose);

// bind connection to error event (to get notification of connection errors)
mongoose.connection.on(
  'error',
  console.error.bind(console, 'MongoDB connection error:')
);

require('./config/passport')(passport);

app.use(compression()); // compress all routes
app.use(helmet());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({ secret: process.env.SESSION_SECRET, resave: false, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

// before routes so they have the variable
app.use(function(req, res, next) {
  res.locals.currentUser = req.user;
  next();
});

app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/compendium', compendiumRouter); // add compendium routes to middleware chain

app.route('/images/:id').get((req, res, next) => {
  const dl = mongoDB.gridFSBucket
    .openDownloadStreamByName(req.params.id.toString())
    .on('error', err => {
      err.status = 404;
      return next(err);
    });

  dl.pipe(res);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
