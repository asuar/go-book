const mongoose = require('mongoose');
const moment = require('moment');

const { Schema } = mongoose;

const MatchSchema = new Schema({
  date_of_match: { type: Date },
  winner: { type: Schema.Types.ObjectId, ref: 'Player' },
  winner_score_difference: { type: String, required: false, max: 5 },
  white_player: { type: Schema.Types.ObjectId, ref: 'Player' },
  black_player: { type: Schema.Types.ObjectId, ref: 'Player' },
  description: { type: String, required: false, max: 100 },
  match_event: { type: String, required: false, max: 100 },
  image_file: { type: String, required: false, max: 100 },
});

// virtual for matches' URL
MatchSchema.virtual('url').get(function() {
  return `/compendium/match/${this._id}`;
});

MatchSchema.virtual('match_date_formatted').get(function() {
  return this.date_of_match
    ? moment(this.date_of_match)
        .utc()
        .format('MMM Do, YYYY')
    : '';
});

MatchSchema.virtual('match_date_formatted_form').get(function() {
  return this.date_of_match
    ? moment(this.date_of_match)
        .utc()
        .format('YYYY-MM-DD')
    : '';
});

MatchSchema.virtual('winner_formatted_form').get(function() {
  let winner;
  switch (
    this.winner.toString() // doesnt work when game was vs themselves
  ) {
    case this.white_player.toString():
      winner = 'W';
      break;
    case this.black_player.toString():
      winner = 'B';
      break;
    case null:
      winner = 'D';
      break;
    default:
      winner = '';
      break;
  }
  return winner;
});

// Image location virtual
MatchSchema.virtual('game_image').get(function() {
  return `/images/${this.image_file}`;
});

module.exports = mongoose.model('Match', MatchSchema);
