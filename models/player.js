const mongoose = require('mongoose');
const moment = require('moment');

const { Schema } = mongoose;

const PlayerSchema = new Schema({
  first_name: { type: String, required: true, max: 100 },
  family_name: { type: String, required: true, max: 100 },
  date_of_birth: { type: Date },
  date_of_death: { type: Date },
  country: { type: String, required: true, max: 100 },
  rank: { type: String, required: true, max: 6 },
});

// virtual for player's URL
PlayerSchema.virtual('url').get(function() {
  return `/compendium/player/${this._id}`;
});

// virtual for player's full name
PlayerSchema.virtual('name').get(function() {
  // to avoid errors in cases where a player does not have either a family name or first name
  // we want to make sure we handle the exception by returning an empty string for that case

  let fullName = '';
  if (this.first_name && this.family_name) {
    fullName = `${this.first_name} ${this.family_name}`;
  }
  if (!this.first_name || !this.family_name) {
    fullName = '';
  }

  return fullName;
});

// virtual for player's lifespan
PlayerSchema.virtual('lifespan').get(function() {
  return `${this.date_of_birth_formatted} - ${this.date_of_death_formatted}`;
});

PlayerSchema.virtual('date_of_birth_formatted').get(function() {
  return this.date_of_birth
    ? moment(this.date_of_birth)
        .utc()
        .format('MMMM Do, YYYY')
    : '';
});

PlayerSchema.virtual('date_of_death_formatted').get(function() {
  return this.date_of_death
    ? moment(this.date_of_death)
        .utc()
        .format('MMMM Do, YYYY')
    : '';
});

PlayerSchema.virtual('date_of_birth_formatted_form').get(function() {
  return this.date_of_birth
    ? moment(this.date_of_birth)
        .utc()
        .format('YYYY-MM-DD')
    : '';
});

PlayerSchema.virtual('date_of_death_formatted_form').get(function() {
  return this.date_of_death
    ? moment(this.date_of_death)
        .utc()
        .format('YYYY-MM-DD')
    : '';
});

module.exports = mongoose.model('Player', PlayerSchema);
