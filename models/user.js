const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const { Schema } = mongoose;

const UserSchema = new Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
});

UserSchema.methods.generateHash = function(password) {
  return bcrypt.hash(password, 12, null);
};

UserSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};

// virtual for player's URL
UserSchema.virtual('url').get(function() {
  return `/user/${this._id}`;
});

module.exports = mongoose.model('User', UserSchema);
