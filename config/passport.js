const mongoose = require('mongoose');
const LocalStrategy = require('passport-local');
const bcrypt = require('bcryptjs');

const User = mongoose.model('User');

module.exports = function(passport) {
  passport.use(
    new LocalStrategy((username, password, done) => {
      User.findOne({ username }, (err, user) => {
        if (err) {
          console.log('error');
          return done(err);
        }
        if (!user) {
          console.log('bad username');
          return done(null, false, { msg: 'Incorrect username' });
        }
        bcrypt.compare(password, user.password, (err, res) => {
          if (res) {
            // passwords match! log user in
            console.log('logged in');
            return done(null, user);
          }
          // passwords do not match!
          console.log('fail');
          return done(null, false, { msg: 'Incorrect password' });
        });
      });
    })
  );

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });
};
