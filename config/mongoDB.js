const gridFSBucket = {};
exports.gridFSBucket = gridFSBucket;

exports.init = function(mongoose) {
  // setup default mongoose connection
  const mongoURI = process.env.MONGODB_URI || process.env.MONGODB_URI_DEV;
  // const conn = mongoose.createConnection(mongoURI, { useNewUrlParse: true, useUnifiedTopology: true });
  mongoose.connect(mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const conn = mongoose.connection;

  conn.once('open', () => {
    console.log('image bucket made');
    // Init stream
    this.gridFSBucket = new mongoose.mongo.GridFSBucket(conn.db, {
      bucketName: 'uploads',
    });
  });
};
