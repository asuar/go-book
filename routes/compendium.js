const express = require('express');

const router = express.Router();

// controller modules
const matchController = require('../controllers/matchController');
const playerController = require('../controllers/playerController');

// Player Routes

// get compendium home page
router.get('/', playerController.index);

// GET for creating player. MUST come BEFORE routes that use player id
router.get('/player/new', playerController.player_create_get);

// POST for creating player
router.post('/player/create', playerController.player_create_post);

// GET for delete player
router.get('/player/:id/delete', playerController.player_delete_get);

// POST for delete player
router.post('/player/:id/delete', playerController.player_delete_post);

// GET to update player
router.get('/player/:id/edit', playerController.player_update_get);

// POST to update player
router.post('/player/:id/update', playerController.player_update_post);

// GET for one player
router.get('/player/:id', playerController.player_detail);

// GET for list of all players
router.get('/players', playerController.player_list);

// Match Routes

// GET for creating match. MUST come BEFORE routes that use match id
router.get('/match/create', matchController.match_create_get);

// POST for creating match
router.post('/match/create', matchController.match_create_post);

// GET for delete match
router.get('/match/:id/delete', matchController.match_delete_get);

// POST for delete match
router.post('/match/:id/delete', matchController.match_delete_post);

// GET for update match
router.get('/match/:id/update', matchController.match_update_get);

// POST for update match
router.post('/match/:id/update', matchController.match_update_post);

// GET for one match
router.get('/match/:id', matchController.match_detail);

// GET for list of all matches
router.get('/matches', matchController.match_list);

module.exports = router;
