const express = require('express');

const router = express.Router();
const passport = require('passport');
const userController = require('../controllers/userController');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/sign-up', userController.user_create_get);

router.post(
  '/sign-up',
  userController.user_create_post,
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/sign-up',
    failureFlash: true,
  })
);

router.get('/login', userController.user_login_get);

router.post(
  '/login',
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true,
  }),
  userController.user_login_post
);

router.get('/logout', userController.logout_get);

module.exports = router;
